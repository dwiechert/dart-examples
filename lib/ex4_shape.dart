import 'dart:math';

abstract class Shape {
  factory Shape(String type, num size) {
    if (type == 'circle') return Circle(size);
    if (type == 'square') return Square(size);
    throw "Can't create $type.";
  }

  num get area;
}

class Circle implements Shape {
  final num radius;
  Circle(this.radius);

  num get area => pi * pow(radius, 2);
}

class Square implements Shape {
  final num side;
  Square(this.side);

  num get area => pow(side, 2);
}

Shape shapeFactory(String type, num size) {
  if (type == 'circle') return Circle(size);
  if (type == 'square') return Square(size);
  throw "Can't create $type.";
}

void main() {
  // Create class directly
  final circle = Circle(2);
  final square = Square(2);
  print(circle.area);
  print(square.area);

  // Create class with function
  final c2 = shapeFactory('circle', 3);
  final s2 = shapeFactory('square', 3);
  print(c2.area);
  print(s2.area);
  try {
    shapeFactory('rectangle', 3);
  } catch (err) {
    print(err);
  }

  // Create class with factory constructor
  final c3 = Shape('circle', 4);
  final s3 = Shape('square', 4);
  print(c3.area);
  print(s3.area);
  try {
    Shape('rectangle', 4);
  } catch (err) {
    print(err);
  }
}