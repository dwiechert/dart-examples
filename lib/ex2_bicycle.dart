// Copied from - https://codelabs.developers.google.com/codelabs/from-java-to-dart/#1

class Bicycle {
  int cadence;
  int gear;
  // Starting with underscore makes it private
  int _speed = 0;
  // Getter for speed
  int get speed => _speed;

  // Constructor for this class
  // Using "this" in method removes initialization code
  Bicycle(this.cadence, this.gear);

  void applyBrake(int decrement) {
    _speed -= decrement;
  }

  void speedUp(int increment) {
    _speed += increment;
  }

  // () => removes bloat for one-line functions
  // Both single and double quotes are allowed
  @override
  String toString() => 'Bicycle: $speed mph';
}

// Optionally - main(List<String< args)
void main() {
  // Optionally - final bike = new Bicycle(2, 0, 1)
  var bike = Bicycle(2, 1);
  print(bike);

  bike.speedUp(10);
  print(bike);

  bike.applyBrake(5);
  print(bike);
}