import 'dart:math';

abstract class Shape {
  num get area;
}

class Circle implements Shape {
  final num radius;
  Circle(this.radius);

  num get area => pi * pow(radius, 2);
}

// Every class is an interface and can be overridden
// Every public method needs to be overridden when implementing
class CircleMock implements Circle {
  num area;
  num radius;
}

void main() {
  final circle = CircleMock();
  print(circle.area);
  print(circle.radius);
}